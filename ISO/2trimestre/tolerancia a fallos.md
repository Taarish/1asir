##Tolerancia a fallos de software de los datos.
---

####Definición



Para mejorar la fiabilidad de un sistema se puede hacer:
1. Prevención de fallos - se usan métodos para impedir que se presenten fallos en los componentes. Los métodos pueden ser de diseño, de test y de validación.

Ejemplo: Si el ventilador de un servidor hace mucho ruido cambiarlo antes de que se produzca un error de sistema mayor.

2. Tolerancia a fallos - Es una técnica complementaria de la prevención. La tolerancia a fallos usa redundancia protectora para enmascarrar fallos. Si un componente falla se usa uno redundante.

Ejemplo: Fuentes de alimentación redundantes, sistemas RAID.

El desarrollo más importante en sistemas tolerantes a fallos son los sistemas distribuidos.

#####Disponibilidad y tolerancia a fallos aplicado a una empresa.

Puntos a tomar en cuenta:

    Infraestructura, en relación a suministros eléctricos y sistemas de alimentación.

    Servidores

    Arquitectura Web

    Seguridad
