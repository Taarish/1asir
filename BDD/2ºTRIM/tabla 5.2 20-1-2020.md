###Tarea 5.2

---

**Ejercicio 6**

`ALTER TABLE PEDIDOS MODIFY UNIDADES_PEDIDAS NUMBER(6);`

`ALTER TABLE VENTAS MODIFY UNIDADES_VENDIDAS NUMBER(6);`

---
**Ejercicio 7**


`
    ALTER TABLE TIENDAS MODIFY PROVINCIA CHECK
    (UPPER(PROVINCIA) <> 'TOLEDO');
`

`INSERT INTO TIENDAS (NOMBRE,PROVINCIA) VALUES ('CYA','TOLEDO');`

---
**Ejercicio 8**

`ALTER TABLE PEDIDOS ADD PVP NUMBER(3);`

`ALTER TABLE VENTAS ADD PVP NUMBER(3)`

